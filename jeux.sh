#! /usr/bin/env bash

read -p "Appuyez sur J (majuscule) pour commencer à jouer et sur CTRL+C ou Q (majuscule) pour quitter " reponse
while test "$reponse" != "J" && test "$reponse" != "Q" 
do 
    read -p "Appuyez sur J (majuscule) pour commencer à jouer et sur CTRL+C ou Q (majuscule) pour quitter " reponse
done
if [ "$reponse" = "J" ]
then 
    cd ./src
    ./main.sh
fi
