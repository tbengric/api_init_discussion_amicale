#! /usr/bin/env bash

chemin="debut"

while [ "$(ls "$chemin"/* | wc -l)" != 1 ]
do
    echo 
    echo "________________________________________________________"
    cat "$chemin"/fichier.txt
    echo "________________________________________________________"
    echo 
    read -p "> Choisissez un numéro de réponse " reponse
    echo
    while [ "$(ls -d "$chemin"/* | grep "$reponse" | wc -l)" = 0 ]
    do
        echo 
        read -p "> Veuillez choisir une réponse valide " reponse
    done
    chemin="$chemin"/"$reponse"
done
echo "________________________________________________________"
echo "$(cat "$chemin"/fin.txt)"
echo "________________________________________________________"
echo 
echo "Cette fin te satisfait ? Si non, retente ta chance ! "




