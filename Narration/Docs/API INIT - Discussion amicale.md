API INIT - Discussion amicale
===

# Notes générales
- idée originale : jeu où l'on discute un·e ami·es qui s'est embrouillé·e avec son·sa partenaire 
    - un peu chiant comme scénar? Ca manque d'originalité, de trucs qui fait qu'on a envie d'y jouer ?
    - Quelle ambiance ? Intimiste ou drôle ? 
- gestion de deux discussions à la fois
    - peut on passer librement d'une à l'autre, où c'est une possibilité restreinte proposée parmi les autres choix de réponses
    - techniquement des encadrants m'ont recommandés de faire que une personne à la fois pour commencer
- Types de fins possibles
    - réflexions
        - faire des fins fun et/ou des fins réalistes?
    - 


# TO DO
## TODO techniques
- Définir comment représenter en donnée une question avec ses réponses
- Construire le code qui permet de naviguer entre les différentes étapes
- Gérer l'affichage du texte et des choix sur le terminal
- Définir l'organisation des fichiers
 
## TODO narration
- définition de l'histoire globale, mécanismes narratifs
- définir les différentes fins possibles
- définir l'arborescence de façon approximative (grands passages, embranchements approximatifs)
- définir l'arboresence de façon précise (intitulé de tout les passages donnés)
- répartition des différents passages à différentes personnes pour la rédaction de leur contenu
- rédaction des passages
- review à plusieurs des passages rédigés, pour s'assurer que tout est ok, discuter de petits trucs (à faire en plusieurs micro réu spontanée)
- validation finale de tout les passages rédigés

## TO DO mise en commun
- réu après premier brief de sous-équipe 
- réu après la fin de rédaction des passages : présenter la narration à l'équipe technique, clarifier à l'équipe narration sous quel forme elle doit forn







# Narration

## sprint 1 :  définition globale de l'histoire

* Contexte général
    - > on discute avec un·e ami·es qui s'est embrouillé·e avec son·sa partenaire 
        - une romance ou juste une amitié ? 
    - on essaye d'obtenir un secret 
    - > une discussion qui dérape avec un·e pote
* personnages
    - notre pote fille Clara a qui on parle : 
    - notre pote mec Pierre avec qui l'autre s'est embrouillé 
    - l'utilisateur mec 
* début
    - 
* Personnalités typiques  des types de réponses
    - qui dénigre Pierre, créer le conflit, qui met l'huile sur le feu, qui est direct et pas très fin
    - qui dénigre sa pote, créer le conflit, qui met l'huile sur le feu, qui est direct et pas très fin
    - qui pose beaucoup de questions, qui n'a pas d'opinions en soit, qui accompagne son amie dans développer une opinion
    - le paillasson, qui dit rien, juste qui écoute ("je vois")
    - qui fait des blagues, qui fuit dans l'humour ("un de perdu, dix de retrouvé")
* Fins possibles
    -    
        - faire des fins sérieuses ou aussi déconnantes ? 
        - enjeux:
            - rester ami avec Clara
            - rester ami avec Pierre
            - que Clara et Pierre reste ami·s si Pierre est qqn de sain
    - Nous, Clara et Pierre restons ami·es, Pierre est qqn de bien
    - Nous, Clara et Pierre restons ami·es, Pierre est qqn de toxique
    - Nous et Clara restons ami·es
    - Nous et Pierre restons amis
    - ...
* trouver l'outil d'écriture pour team narration logiciel libre d'arborescence ou équivalent figma
* idées
    - nb de réponses variables
    - avoir des infos que graĉe à certains chemins
    - pistes d'approfondissement techniques :
        -  pouvoir choisir le prénom et le genre du personnage (voir avec team technique)
    - à quel point on a accès aux pensées de notre personnage ? 
    - si on fait tout le temps la même personnalité, on perd : la morale c'est qu'il faut s'adapter, être pertinent
    - que ce soit l'utilisateur qui décide de la toxicité de Pierre 

## Sprint 2 : 




