# Projet Discussion amicale

## Description 

Projet scolaire réalisé dans le cadre de l'API Init à l'UTC en janvier 2023. Membres du projet : Morgane Rigaud, Dimitri Perigois, Naomi Hure, Tidiane Bengriche. Tous droits réservés.

## Comment installer 

Exécuter cette commande dans un terminal 
```git clone git@gitlab.utc.fr:tbengric/api_init_discussion_amicale.git```
ou
```git clone  https://gitlab.utc.fr/tbengric/api_init_discussion_amicale.git```

## Comment jouer

Pour jouer il vous faut exécuter dans un terminal 
```
cd api_init_discussion_amicale
bash ./jeux.sh
```

## Liscence 

Liscence libre MIT

